
/* ----------Bài 1 ---------- */
function ngayHomQua(){
    var dd = document.getElementById("txt-ngay").value*1;
    var mm = document.getElementById("txt-thang").value*1;
    var yy = document.getElementById("txt-nam").value*1;
    if(dd==1&&mm==1){
        dd+=31;
        mm=12;
        yy-=1;
    }
    if(dd==1){
        switch(mm){
            case 5: case 7: case 10: case 12:
                mm-=1;
                dd+=30;
                break;
            case 1: case 2: case 4: case 6: case 8: case 9: case 11:
                dd+=31;
                mm-=1;
                break;
            case 3:
                mm-=1;
                if(yy%400==0 || (yy%4==0 && yy%100!=0)){
                    dd+=29;
                }
                else{
                    dd+= 28;
                }
                break;
        }
    }
    dd--;
    var result1El = document.getElementById("txt-hien-thi").setAttribute('value',`${dd}/${mm}/${yy}`);
    if(dd<0 || dd>31 || mm<0 || mm>12 ||yy<0){
        alert("Dữ liệu không hợp lệ!");
        var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
    }
    if(yy<=1900){
        alert("Vui lòng nhập năm lớn hơn 1900!");
        var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
    }
}

function ngayMai(){
    var dd = document.getElementById("txt-ngay").value*1;
    var mm = document.getElementById("txt-thang").value*1;
    var yy = document.getElementById("txt-nam").value*1;

    if(dd==31&&mm==12){
        dd=0;
        mm=1;
        yy+=1;
    }

    if(dd == 31){
        if(mm== 1 || mm== 3 || mm== 5 || mm== 7 || mm== 8 || mm== 10 || mm== 12){
            dd=0;
            mm+=1;
        }
    }
    if(dd == 30){
        if(mm == 4 || mm == 6 || mm == 9 || mm == 11){
            dd=0;
            mm+=1;
        }
    }

    if(mm == 2){
        if(dd == 29){
            if(yy%400==0 || (yy%4==0 && yy%100!=0)){
                dd=0;
                mm++;
            }
            else{
                alert("Dữ liệu không hợp lệ");
                var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
            }
        }
        if(dd == 28){
            dd=0;
            mm++;
            console.log("🚀 ~ file: index.js:75 ~ ngayMai ~ mm", dd, mm, yy);
            if(dd>=29)
            {
                alert("Dữ liệu không hợp lệ");
                var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
            }
        }
    }
    dd++;
    var result1El = document.getElementById("txt-hien-thi").setAttribute('value',`${dd}/${mm}/${yy}`);

    if(mm==2 && dd > 29){
        var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
    }
    if(dd<0 || dd>31 || mm<0 || mm>12 ||yy<0){
        alert("Dữ liệu không hợp lệ!");
        var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
    }
    if(yy<=1900){
        alert("Vui lòng nhập năm lớn hơn 1900!");
        var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
    }
}

/* ----------Bài 2 ---------- */

function tinhNgay(){
    var m = document.getElementById("txt-nhap-thang").value*1;
    var y = document.getElementById("txt-nhap-nam").value*1;
    var d;
    switch(m){
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            d = 31;
            break;
        case 4: case 6: case 9: case 11:
            d = 30;
            break;
        case 2:
            if(y%400==0 || (y%4==0 && y%100!=0)){
                d=29;
            }
            else d=28;
    }

    var result1El = document.getElementById("txt-tinh").setAttribute('value',`Tháng ${m} năm ${y} có ${d} ngày`);

    if(m<0 || m>12 ||y<0){
        alert("Dữ liệu không hợp lệ!");
        var result1El = document.getElementById("txt-tinh").setAttribute('value',``);
    }
    if(y<=1900){
        alert("Vui lòng nhập năm lớn hơn 1900!");
        var result1El = document.getElementById("txt-tinh").setAttribute('value',``);
    }
}

/* ----------Bài 3 ---------- */

function docSo(){
    var a = document.getElementById("txt-so").value*1;
    var dv, c, t;
    var donVi = a%10;
    var hangChuc = Math.floor(a/10)%10;
    var hangTram = Math.floor((a/10)/10);
    switch(donVi){
        case 1:
            dv = 'mốt';
            break;
        case 2:
            dv = 'hai';
            break;
        case 3:
            dv = 'ba';
            break;
        case 4:
            dv = 'bốn';
            break;
        case 5:
            dv = 'năm';
            break;
        case 6:
            dv = 'sáu';
            break;
        case 7:
            dv = 'bảy';
            break;
        case 8:
            dv = 'tám';
            break;
        case 9:
            dv = 'chín';
            break;
        default:
            dv ='';
    }
    switch(hangChuc){
        case 1:
            c = "mười";
            break;
        case 2:
            c = "hai mươi";
            break;
        case 3:
            c = "ba mươi";
            break;
        case 4:
            c = "bốn mươi";
            break;
        case 5:
            c = "năm mươi";
            break;
        case 6:
            c = "sáu mươi";
            break;
        case 7:
            c = "bảy mươi";
            break;
        case 8:
            c = "tám mươi";
            break;
        case 9:
            c = "chín mươi";
            break;
        default:
            c ="";
    }
    switch(hangTram){
        case 1:
            t = "một trăm";
            break;
        case 2:
            t = "hai trăm";
            break;
        case 3:
            t = "ba trăm";
            break;
        case 4:
            t = "bốn trăm";
            break;
        case 5:
            t = "năm trăm";
            break;
        case 6:
            t = "sáu trăm";
            break;
        case 7:
            t = "bảy trăm";
            break;
        case 8:
            t = "tám trăm";
            break;
        case 9:
            t = "chín trăm";
            break;
        default:
            t = "";
    }
    var result3El = document.getElementById("txt-doc-so").setAttribute('value',`${t} ${c} ${dv}`);
    if(a<100 || a>999){
        alert("Dữ liệu không hợp lệ!");
        var result1El = document.getElementById("txt-doc-so").setAttribute('value',``);
    }
}

/* ----------Bài 4 ---------- */

function timSV(){
    var localX = document.getElementById("txt-x-th").value*1;
    var localY = document.getElementById("txt-y-th").value*1;

    var varname1 = document.getElementById("txt-ten-sv1").value;
    var localX1 = document.getElementById("txt-x-sv1").value*1;
    var localY1 = document.getElementById("txt-y-sv1").value*1;
    var tmp1 = Math.pow((localX-localX1),2)+Math.pow((localY-localY1),2);
    var local1 = Math.sqrt(tmp1);

    var varname2 = document.getElementById("txt-ten-sv2").value;
    var localX2 = document.getElementById("txt-x-sv2").value*1;
    var localY2 = document.getElementById("txt-y-sv2").value*1;
    var tmp2 = Math.pow((localX-localX2),2)+Math.pow((localY-localY2),2);
    var local2 = Math.sqrt(tmp2);

    var varname3 = document.getElementById("txt-ten-sv3").value;
    var localX3 = document.getElementById("txt-x-sv3").value*1;
    var localY3 = document.getElementById("txt-y-sv3").value*1;
    var tmp3 = Math.pow((localX-localX3),2)+Math.pow((localY-localY3),2);
    var local3 = Math.sqrt(tmp3);

    if(local1 >= local2 && local1 >= local3){
        var result3El = document.getElementById("txt-tim").setAttribute('value',`Sinh viên xa trường nhất: ${varname1}`);
    }

    else if(local2 >= local1 && local2 >= local3){
        var result3El = document.getElementById("txt-tim").setAttribute('value',`Sinh viên xa trường nhất: ${varname2}`);
    }
    else {
        var result3El = document.getElementById("txt-tim").setAttribute('value',`Sinh viên xa trường nhất: ${varname3}`);
    }


}


function bai1(){
    document.getElementById("e1").style.display = 'block';
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
}

function bai2(){
    document.getElementById("e2").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
}

function bai3(){
    document.getElementById("e3").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e4").style.display = "none";
}

function bai4(){
    document.getElementById("e4").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
}

